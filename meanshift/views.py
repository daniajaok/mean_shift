from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import MeanShift
from pandas_profiling import ProfileReport
import pandas as pd
import json
import os


print('Start Preparation Data ...')

# Data Web
DATA_WEB = {
    'upload_file': False
}

# Uncleaned Data
data_diterima = pd.read_excel(settings.BASE_DIR / 'static/datasets/__DATA COVID 2020.xlsx')

# Cleaned Data
df = pd.read_excel(settings.BASE_DIR / 'static/datasets/__DATA_COVID_2020_v1B.xlsx')
df.dropna()
df['simptomatik'].replace({True:1,False:0},inplace=True)
df['simptomatik'].map({True: 1, False: 0})  
df['Asimptomatik'].replace({True:1,False:0},inplace=True)
df['BULAN_POSITIF'] = pd.DatetimeIndex(df['TANGGAL_POSITIF']).month
df['TAHUN_POSITIF'] = pd.DatetimeIndex(df['TANGGAL_POSITIF']).year
df['BULAN_SEMBUH'] = pd.DatetimeIndex(df['TANGGAL_SEMBUH']).month
df['TAHUN_SEMBUH'] = pd.DatetimeIndex(df['TANGGAL_SEMBUH']).year
df.fillna(0)
df1=df.copy()
df1=df1.drop(['TANGGAL_POSITIF','TANGGAL_SEMBUH'], axis = 1)
def remove_outlier(df):
    Q1=df.quantile(0.25)
    Q3=df.quantile(0.75)
    IQR=Q3-Q1
    df_final=df[~(df>(Q1-(1.5*IQR)))|(df<(Q3+(1.5*IQR)))]
    return df_final
for x in range(2):
    df1=remove_outlier(df1)
    df1.dropna(axis=0,inplace=True)
df1.to_excel(settings.BASE_DIR / "static/datasets/data_covid_bersihF.xlsx",index=False)
data_cleanup=pd.read_excel(settings.BASE_DIR / 'static/datasets/data_covid_bersihF.xlsx')

# Data Enters To Machine Learning
enter_ml = data_cleanup.copy()
enter_ml.drop(['KELURAHAN','Latitude','Longitude','TAHUN_POSITIF','TAHUN_SEMBUH','JENIS_KELAMIN'], axis=1, inplace=True)
enter_ml["UMUR"] = (enter_ml["UMUR"] - enter_ml["UMUR"].min()) / (enter_ml["UMUR"].max() - enter_ml["UMUR"].min())
enter_ml["simptomatik"] = (enter_ml["simptomatik"] - enter_ml["simptomatik"].min()) / (enter_ml["simptomatik"].max() - enter_ml["simptomatik"].min())
enter_ml["Asimptomatik"] = (enter_ml["Asimptomatik"] - enter_ml["Asimptomatik"].min()) / (enter_ml["Asimptomatik"].max() - enter_ml["Asimptomatik"].min())
enter_ml["Kode Kelurahan"] = (enter_ml["Kode Kelurahan"] - enter_ml["Kode Kelurahan"].min()) / (enter_ml["Kode Kelurahan"].max() - enter_ml["Kode Kelurahan"].min())
enter_ml["BULAN_POSITIF"] = (enter_ml["BULAN_POSITIF"] - enter_ml["BULAN_POSITIF"].min()) / (enter_ml["BULAN_POSITIF"].max() - enter_ml["BULAN_POSITIF"].min())
enter_ml["BULAN_SEMBUH"] = (enter_ml["BULAN_SEMBUH"] - enter_ml["BULAN_SEMBUH"].min()) / (enter_ml["BULAN_SEMBUH"].max() - enter_ml["BULAN_SEMBUH"].min())
enter_ml.to_excel(settings.BASE_DIR / 'static/datasets/normalisasi_MeanShift.xlsx')

# Refresh Data
data_refresh = data_cleanup.copy()
sc = StandardScaler()
df_std = sc.fit_transform(enter_ml)
MeanShiftClustering = MeanShift().fit(df_std)
Meanshift= MeanShiftClustering.labels_
data_refresh['hasil_MeanShift']=Meanshift
data_refresh.to_excel(settings.BASE_DIR / "static/reports/MeanShift.xlsx", index=False)

# Cluster
cluster_result=data_cleanup.copy()
cluster_result['Cluster_MeanShift']=Meanshift

print('Finish Preparation Data ...')

def index(request):
    context = {
        'title_page': 'Upload File',
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def uncleanup_data(request):
    if request.GET.get('upload_file', False):
        DATA_WEB['upload_file'] = True
    title_data = data_diterima.columns
    json_records = data_diterima.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Pre-Cleaned Data',
        'title_data': title_data,
        'content_data': data_csv,
        'cluster_view': False,
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def cleanup_data(request):
    cluster_view = False
    refresh_button = False
    if request.GET.get('refresh_data', False) == '0':
        title_data = enter_ml.columns
        json_records = enter_ml.reset_index().to_json(orient='records')
        refresh_button = True

    elif request.GET.get('refresh_data', False) == '1':
        title_data = data_refresh.columns
        json_records = data_refresh.reset_index().to_json(orient='records')
        refresh_button = True
        cluster_view = True

    else:
        title_data = data_cleanup.columns
        json_records = data_cleanup.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Data After Cleaning',
        'title_data': title_data,
        'content_data': data_csv,
        'refresh_button': refresh_button,
        'cluster_view': cluster_view,
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def excel_report(request):
    if not os.path.exists(settings.BASE_DIR / 'static/reports/MeanShift.xlsx'):
        data_refresh.to_excel(settings.BASE_DIR / "static/reports/MeanShift.xlsx",index=False)
    file_name = 'MeanShift.xlsx'
    file_path = settings.BASE_DIR / 'static/reports/MeanShift.xlsx'
    file = open(file_path, 'rb').read()
    response = HttpResponse(file, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=%s' % (file_name)
    return response

def html_report(request):
    profile = ProfileReport(data_refresh, title="MeanShift")
    profile.to_file(settings.BASE_DIR / "static/reports/MeanShift.html")#hasil download
    file = open(settings.BASE_DIR / "static/reports/MeanShift.html", 'rb').read()
    response = HttpResponse(file, content_type='text/html')
    response['Content-Disposition'] = 'attachment; filename=MeanShift.html'
    return response

def data_cluster(request):
    cluster = request.GET.get('cluster', '0')
    data_cluster = cluster_result[cluster_result['Cluster_MeanShift'] == 0]
    title_page = 'Cluster 0'
    data_query_param = 0

    if cluster == '0':
        title_page = 'Cluster 0'
        data_query_param = 0
        data_cluster = cluster_result[cluster_result['Cluster_MeanShift'] == 0]
    elif cluster == '1':
        title_page = 'Cluster 1'
        data_query_param = 1
        data_cluster = cluster_result[cluster_result['Cluster_MeanShift'] == 1]

    title_data = data_cluster.columns
    json_records = data_cluster.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': title_page,
        'title_data': title_data,
        'content_data': data_csv,
        'refresh_button_cluster': True,
        'data_query_param': data_query_param,
        'cluster_view': True,
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def report_cluster(request):
    cluster = request.GET.get('cluster', '0')
    data_cluster = cluster_result[cluster_result['Cluster_MeanShift'] == 0]
    file_name = 'Cluster_MeanShift0.xlsx'
    file_path = settings.BASE_DIR / 'static/reports'

    if cluster == '0':
        file_name = 'Cluster_MeanShift0.xlsx'
        data_cluster = cluster_result[cluster_result['Cluster_MeanShift'] == 0]
    elif cluster == '1':
        file_name = 'Cluster_MeanShift1.xlsx'
        data_cluster = cluster_result[cluster_result['Cluster_MeanShift'] == 1]
    elif cluster == '2':
        file_name = 'Cluster_MeanShift2.xlsx'
        data_cluster = cluster_result[cluster_result['Cluster_MeanShift'] == 2]

    full_path = '{}/{}'.format(file_path, file_name)
    data_cluster.to_excel(full_path)#hasil download
    file = open(full_path, 'rb').read()
    response = HttpResponse(file, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=%s' % (file_name)
    return response
        

