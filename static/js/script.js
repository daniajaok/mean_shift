const navDropdownsLinks = document.querySelectorAll('.navbar__dropdown > a')
const navDropdowns = document.querySelectorAll('.navbar__dropdown')
const page = document.getElementById('page')

const deleteDropdownActive = () => {
    navDropdowns.forEach(navDropdown => {
        navDropdown.classList.remove('active')
    })
}

navDropdownsLinks.forEach(navDropdownLink => {
    navDropdownLink.addEventListener('click', (e) => {
        if (navDropdownLink.parentElement.className.includes('active')){
            navDropdownLink.parentElement.classList.remove('active')
            page.classList.remove('active')
        }else {
            deleteDropdownActive()
            page.classList.add('active')
            navDropdownLink.parentElement.classList.add('active')
        }
    })
})

const input_file = document.getElementById('inputFile')
const display_file = document.getElementById('displayFile')
const drop_area = document.getElementById('drop-area')

const displayFile = () => {
    let file_name = input_file.files[0].name
    display_file.innerHTML = ''

    let paragraph = document.createElement('p')
    paragraph.textContent = file_name
    let btn_close = document.createElement('i')
    btn_close.setAttribute('id', 'delFileElem')
    btn_close.addEventListener('click', (e) => {
        display_file.innerHTML = ''
        input_file.files = null
    })
    btn_close.className = 'bx bx-x'
    
    display_file.append(paragraph)
    display_file.append(btn_close)
    display_file.className = 'active upload'
    setTimeout(() => {
        display_file.classList.remove('upload')
    }, 1000);
}

input_file.addEventListener('change', (e) => {
    displayFile()
})

;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
    drop_area.addEventListener(eventName, (e) => {
        e.preventDefault()
        e.stopPropagation()
    }, false)
})

;['dragenter', 'dragover'].forEach(eventName => {
    drop_area.addEventListener(eventName, (e) => drop_area.classList.add('highlight'), false)
})

;['dragleave', 'drop'].forEach(eventName => {
    drop_area.addEventListener(eventName, (e) => drop_area.classList.remove('highlight'), false)
})

drop_area.addEventListener('drop', (e) => {
    let dt = e.dataTransfer
    let files = dt.files
    input_file.files = files
    displayFile()
}, false)


