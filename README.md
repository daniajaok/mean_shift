# Aplikasi Analisis sebaran COVID-19, Mengunkan metode meanshift di daerah Tasik

Dalam aplikasi menggunakan Django dengan metode Meanshift dan hasil Silhouette Score untuk studi kasus sebaran COVID-19, sistem dapat digunakan untuk menganalisis pola persebaran kasus COVID-19 di suatu wilayah.

Pertama, data kasus COVID-19 dari berbagai lokasi disimpan dalam basis data. Kemudian, dengan menggunakan Django sebagai kerangka kerja pengembangan web, sebuah aplikasi web dibangun untuk memungkinkan pengguna untuk melakukan analisis visual terhadap data tersebut.

Metode Meanshift digunakan untuk mengelompokkan kasus-kasus COVID-19 ke dalam klaster-klaster berdasarkan pola persebarannya. Selanjutnya, hasil dari Meanshift dikuantifikasi menggunakan metrik Silhouette Score untuk mengevaluasi seberapa baik klasterisasi tersebut.

Pada antarmuka aplikasi web Django, pengguna dapat memilih wilayah tertentu untuk dianalisis. Hasil analisis berupa visualisasi pola klasterisasi kasus COVID-19 ditampilkan dalam bentuk peta atau grafik yang mudah dipahami. Informasi tambahan seperti jumlah kasus dalam setiap klaster, serta tingkat kepadatan dan kemiripan antar klaster juga disajikan.

Dengan aplikasi ini, pengguna dapat dengan cepat memperoleh wawasan tentang pola persebaran COVID-19 di wilayah yang dipilih, serta mengevaluasi efektivitas metode Meanshift dalam mengelompokkan kasus-kasus tersebut.

### Note 
jika anda ingin mejalankanya aplikasi tolong hubingi saya [Muhammad Ramdhani](wa.me/+6281399057525) karena aplikasi ini ada data privasi yang tidak saya sertakan pada repository ini